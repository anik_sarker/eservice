<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
		    $table->engine = 'InnoDB';
			$table->increments("id");
            $table->string("fullname", 255);
            $table->string("email", 255)->unique();
            $table->string("password", 120);
            $table->integer("skill");
            $table->string("phone", 255);
            $table->string("photo", 255);
            $table->date("birthdate");
            $table->enum('gender', array('male','female','other'));
            $table->text("address");
            $table->text("aboutme");
            $table->text("latitude");
            $table->text("longitude");
            $table->dateTime('last_login');
            $table->enum('is_admin',array(0,1))->default(0);
            $table->enum('status', array('active','deactive','ban'));
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->dropIfExists();
		});
	}

}
