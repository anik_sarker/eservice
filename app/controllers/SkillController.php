<?php

class SkillController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	    
	    try {
	    	$limit = 25;
	        if(Input::has('limit')) {	    		
	    		$limit = Input::get('limit');
	    		if($limit == 'all') {
		    		$limit = Skill::all()->count();	        	
	    		}
	    	}

	    	if(Input::has('search')){
	    		$search = Input::get('search');
	    		$skills = Skill::where('name', 'like', "%$search%")->paginate($limit);	    		
	    	} else {
	    		$skills = Skill::paginate($limit);
	    	}

	        $response = array(
			    'data'   => $skills->getItems(),
			    'pagination' => array(
			        'total'        => $skills->getTotal(),
			        'per_page'     => $skills->getPerPage(),
			        'current_page' => $skills->getCurrentPage(),
			        'last_page'    => $skills->getLastPage(),
			        'from'         => $skills->getFrom(),
			        'to'           => $skills->getTo()
			    )
			);
			
	        if($skills->getTotal()) {
	            $this->_response['skills'] = $response;
	            $this->_response['response'] = 'Skill category found';
	        } else {
	            $this->_response['skills'] = null;
	            $this->_response['response'] = 'Skill category not found';
	        }
	    
	    }catch (Exception $e) {
	        $this->_status = 400;
	        $this->_response['error'] = true;
	        $this->_response['response'] = $e;
	    }
	    
	    return Response::json($this->_response, $this->_status);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$skill = new Skill();
		$input = Input::all();
		
		if(Input::has('name')){
		    $skill->name = trim($input['name']);
		    $skill->parent = (Input::has('parent')) ? $input['parent'] : 0;
		    
		    if($skill->save()) {
		        $this->_response['response'] = 'Skill category is created';
		        $this->_response['skills']     = $skill->toArray();
		    } else {
		        $this->_response['response'] = 'Skill category unable to create';
		        $this->_response['error'] = true;
		    }
		} else {
		    $this->_response['response'] = '"name" field required';
		    $this->_response['error'] = true;
		}

		return Response::json( $this->_response, $this->_status);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	    try{
	        $skill = Skill::find($id);	        
	        if(!empty($skill)) {
	            $this->_response['skills'] = $skill->toArray();
	            $this->_response['response'] = 'Skill category found';
	        } else {
	            $this->_response['skills'] = null;
	            $this->_response['response'] = 'Skill category not found';
	        }
	    
	    }catch(\Exception $e){
	        $this->_response['error'] = true;
	        $this->_response['response'] = $e;
	        $this->_status = 404;
	    }
	    
	    return Response::json($this->_response, $this->_status);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		try{
	        $skill = Skill::find($id);	        
	        if(!empty($skill)) {
	            $input = Input::all();
	            if(Input::has('name') || Input::has('parent')){
	                $skill->name = (Input::has('name')) ? $input['name'] : $skill->name;
	                $skill->parent = (Input::has('parent')) ? $input['parent'] : $skill->parent;
	                if($skill->save()){
	                    $this->_response['response'] = 'Skill category updated';
	                    $this->_response['skills'] = $skill->toArray();
	                } else {
	                    $this->_response['error'] = true;
	                    $this->_response['response'] = 'Skill category do not updated';
	                    $this->_response['skills'] = $skill->toArray();
	                }
	            } else {
	                $this->_response['error'] = true;
	                $this->_response['response'] = 'No field found';
	            }
	        } else {
	            $this->_response['error'] = true;
	            $this->_response['response'] = 'Skill category not found';
	        }
	    
	    }catch(\Exception $e){
	        $this->_response['error'] = true;
	        $this->_response['response'] = $e;
	        //$this->_status = 404;
	    }
	    
	    return Response::json($this->_response, $this->_status);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	    try{
	        $skill = Skill::find($id);
	        if(!empty($skill)) {
	            $affectedRows = Skill::where('parent','=',$id)->delete();
	            $skill->delete();
	            $this->_response['error'] = false;
	            $this->_response['response'] = 'Skill category removed and '.$affectedRows.' subcategory removed';
	        } else {
	            $this->_response['error'] = true;
	            $this->_response['response'] = 'Skill category not found';
	        }
	         
	    }catch(Exception $e){
	        $this->_response['error'] = true;
	        $this->_response['response'] = $e;
	        $this->_status = 404;
	    }
	     
	    return Response::json($this->_response, $this->_status);
	}


}
