<?php

class BaseController extends Controller {
    
    public $_status     = 200;
    public $_response   = array(
            'error' => false,
            'response' => ''
        );

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
