<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		try {			
	    	$search = null;
	    	$skill = null;
			$limit = 25;
	    	
	    	if(Input::has('limit')) {	    		
	    		$limit = Input::get('limit');
	    		if($limit == 'all') {
		    		$limit = User::all()->count();	        	
	    		}
	    	}	    	

	    	if(Input::has('search')){
	    		$search = Input::get('search');	    		
	    	}

	    	if(Input::has('skill')){
	    		$skill = (Input::get('skill')!='all') ? Input::get('skill') : null;
	    	}

	    	if(Input::has('search') && Input::has('skill')) {
	    		$users = User::search($search)->userSkill($skill)->with('skill')->paginate($limit);
	    	} else {
	    		if(Input::has('search')) {
	    			$users = User::search($search)->with('skill')->paginate($limit);
	    		} else if($skill){
	    			$users = User::userSkill($skill)->with('skill')->paginate($limit);
	    		} else {
	    			$users = User::with('skill')->paginate($limit);
	    		}
	    	}

        	$response = array(
			    'data'   => $users->getItems(),
			    'pagination' => array(
			        'total'        => $users->getTotal(),
			        'per_page'     => $users->getPerPage(),
			        'current_page' => $users->getCurrentPage(),
			        'last_page'    => $users->getLastPage(),
			        'from'         => $users->getFrom(),
			        'to'           => $users->getTo()
			    )
			);      
	        
	        if($users->getTotal()) {
	            $this->_response['users'] = $response;
	            $this->_response['response'] = 'Users found';
	        } else {
	            $this->_response['users'] = [];
	            $this->_response['response'] = 'User not found';
	        }
	    
	    }catch (\Exception $e) {
	        $this->_status = 400;
	        $this->_response['error'] = true;
	        $this->_response['response'] = $e;
	    }
	    
	    return Response::json($this->_response, $this->_status);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$input = Input::all();

		$rules = array(
			'fullname' 			=> 'required',
			'email'            	=> 'required|email|unique:users',
			'password'         	=> 'required|min:6',
			'password_confirm' 	=> 'required|same:password',
			'skill'				=> 'required'
		);

		$validator = Validator::make($input, $rules);
		if($validator->fails()) {
			$this->_response['error'] 		= true;
			$this->_response['response'] 	= 'validation error';
			$this->_response['message'] 	= $validator->messages();
		} else {
			$user 				= new User();
			$user->fullname 	= $input['fullname'];
			$user->email 		= $input['email'];
			$user->password 	= Hash::make($input['password']);
			$user->skill 		= $input['skill'];
			$user->birthdate 	= $input['birthdate'];
			$user->phone		= isset($input['phone']) ? $input['phone'] :'';
			$user->address		= isset($input['address']) ? $input['address'] :'';

			try {
				$user->save();
				$this->_response['response'] 	= "new user created";
				$this->_response['users'] 		= $user->toArray();
			} catch(\Exception $e){
				$this->_response['error'] 		= true;
				$this->_response['response'] 	= "user not created";
				$this->_response['messages'] 	= $e;
			}
		}

		return Response::json( $this->_response, $this->_status);

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
	        $user = User::find($id);
	        if(!empty($user)) {
	            $this->_response['users'] = $user->toArray();
	            $this->_response['users']['skill'] = $user->skill()->first();
	            $this->_response['response'] = 'user found';
	        } else {
	            $this->_response['users'] = null;
	            $this->_response['response'] = 'user not found';
	        }
	    
	    }catch(\Exception $e){
	        $this->_response['error'] = true;
	        $this->_response['response'] = $e;
	        $this->_status = 404;
	    }
	    
	    return Response::json($this->_response, $this->_status);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input 	= Input::all();
		$user 	= User::find($id);

		$user->fullname 	= isset($input['fullname']) ? $input['fullname'] : $user->fullname;
		$user->email 		= isset($input['email']) ? $input['email'] : $user->email;
		$user->password 	= isset($input['password']) ? Hash::make($input['password']) : $user->password;
		$user->skill 		= isset($input['skill']) ? $input['skill']['id'] : $user->skill;
		$user->phone		= isset($input['phone']) ? $input['phone'] : $user->contact_no;
		$user->address		= isset($input['address']) ? $input['address'] : $user->address;
		$user->birthdate	= isset($input['birthdate']) ? $input['birthdate'] : $user->birthdate;

		if (Input::has('photo')) {
			$image_string = $input['photo'];
			if (!preg_match('/uploads/',$image_string)):
				if(!is_dir(public_path().'/uploads')){
					mkdir(public_path().'/uploads');
				}
				$imgData = explode(',',$image_string);
				if(count($imgData) == 2) {
					list($extra_info, $encoded_base64image) = explode(',',$image_string);
					if (base64_decode($encoded_base64image, true) == true){
						list(,$type) = explode('/',$extra_info);
						list($type,) = explode(';',$type);

						$decode_base64image = base64_decode($encoded_base64image);
						$imName = preg_replace('/[^A-Za-z0-9\-]/', '_', $user->email);
						$image_name= $imName.'.'.$type;
						$path = public_path() . "/uploads/" . $image_name;

						file_put_contents($path, $decode_base64image);
						$user->photo = "uploads/" . $image_name;
					}
				}
			endif;

		}

		try {
			$user->save();
			$this->_response['response'] = "User update successfully";
			$this->_response['users'] = $user->toArray();
		} catch(\Exception $e){
			$this->_response['error'] = true;
			$this->_response['response'] = "Unable to update user";
			$this->_response['messages'] = $e;
		}

		return Response::json( $this->_response, $this->_status);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);

		try {
			$user->delete();
			$this->_response['response'] = "user deleted";
		} catch (\Exception $e) {
			$this->_response['error'] = true;
			$this->_response['response'] = "user not deleted";
			$this->_response['messages'] = $e;
		}
	}

}
