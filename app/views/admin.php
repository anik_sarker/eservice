<!DOCTYPE html>
<html lang="en" data-ng-app="app">
  <head>
    <meta charset="utf-8" />
    <title>Easy Service | Admin panel</title>
    <meta name="description" content="app, web app, responsive, responsive layout, admin, admin panel, admin dashboard, flat, flat ui, ui kit, AngularJS, ui route, charts, widgets, components" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="<?php echo asset('app/admin/assets/css/bootstrap.css');?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo asset('app/admin/assets/css/animate.css');?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo asset('app/admin/assets/css/font-awesome.min.css');?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo asset('app/admin/assets/css/simple-line-icons.css');?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo asset('app/admin/assets/css/font.css');?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo asset('app/admin/assets/css/app.css');?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo asset('ng-libs/modules/ng-loading-bar/loading-bar.min.css');?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo asset('ng-libs/modules/ng-notification/angular-ui-notification.min.css');?>" type="text/css" />
  </head>
  <body ng-controller="AppCtrl">
    <div class="app" id="app" ng-class="{'app-header-fixed':app.settings.headerFixed, 'app-aside-fixed':app.settings.asideFixed, 'app-aside-folded':app.settings.asideFolded}" ui-view></div>
    <!-- jQuery -->
    <script src="<?php echo asset('ng-libs/jquery/jquery.min.js');?>"></script>
    <!-- Angular -->
    <script src="<?php echo asset('ng-libs/angular/angular.min.js');?>"></script>
    <script src="<?php echo asset('ng-libs/angular/angular-cookies.min.js');?>"></script>
    <script src="<?php echo asset('ng-libs/angular/angular-animate.min.js');?>"></script>
    <script src="<?php echo asset('ng-libs/angular/angular-ui-router.min.js');?>"></script>
    <script src="<?php echo asset('ng-libs/angular/angular-translate.js');?>"></script>
    <script src="<?php echo asset('ng-libs/angular/angular-resource.min.js');?>"></script>
    <script src="<?php echo asset('ng-libs/angular/ngStorage.min.js');?>"></script>
    <script src="<?php echo asset('ng-libs/angular/ui-load.js');?>"></script>
    <script src="<?php echo asset('ng-libs/angular/ui-jq.js');?>"></script>
    <script src="<?php echo asset('ng-libs/angular/ui-validate.js');?>"></script>
    <script src="<?php echo asset('ng-libs/angular/ui-bootstrap-tpls.min.js');?>"></script>
    <script src="<?php echo asset('ng-libs/modules/ng-loading-bar/loading-bar.min.js');?>"></script>
    <script src="<?php echo asset('ng-libs/modules/ng-notification/angular-ui-notification.min.js');?>"></script>
    <!-- App -->
    <script src="<?php echo asset('app/admin/app.js');?>"></script>
    <script src="<?php echo asset('app/admin/services/services.js');?>"></script>
    <script src="<?php echo asset('app/admin/controllers/controllers.js');?>"></script>
    <script src="<?php echo asset('app/admin/filters/filters.js');?>"></script>
    <script src="<?php echo asset('app/admin/directives/directives.js');?>"></script>
    <script src="<?php echo asset('app/admin/controllers/dashboard.js');?>"></script>
    <script src="<?php echo asset('app/admin/controllers/user.js');?>"></script>
    <script src="<?php echo asset('app/admin/controllers/skill.js');?>"></script>
    <script src="<?php echo asset('app/admin/controllers/login.js');?>"></script>
    <!-- Lazy loading -->
    <div cg-busy="myPromise"></div>
  </body>
</html>