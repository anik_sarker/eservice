<!DOCTYPE html>
<html data-ng-app="app">
    <head>
        <meta charset="utf-8" />
        <title>EasyService</title>
        <meta name="description" content="app, web app, responsive, responsive layout, flat, flat ui, ui kit, AngularJS, ui route, charts, widgets, components" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="<?php echo asset('app/frontend/assets/css/bootstrap.min.css');?>" type="text/css" />        
        <link rel="stylesheet" href="<?php echo asset('app/frontend/assets/css/animate.css');?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset('app/frontend/assets/css/font-awesome.min.css');?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset('app/frontend/assets/css/simple-line-icons.css');?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset('app/frontend/assets/css/font.css');?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset('app/frontend/assets/css/style.css');?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo asset('ng-libs/modules/ng-loading-bar/loading-bar.min.css');?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo asset('ng-libs/modules/ng-notification/angular-ui-notification.min.css');?>" type="text/css" />
    </head>

    <body ng-controller="AppCtrl">
        
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" ng-click="isCollapsed = !isCollapsed" data-toggle="collapse" data-target="#es-navbar-collapse-main">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Easy Service</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="es-navbar-collapse-main" collapse="isCollapsed">
                    <ul class="nav navbar-nav">
                        <li ui-sref-active="active">
                            <a ui-sref="home" href="#">Home</a>
                        </li>                        
                        <li ui-sref-active="active">
                            <a ui-sref="contact" href="#">Contact</a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right" ng-hide="isLogin" class="ng-hide">
                      <li ui-sref-active="active" ><a ui-sref="registration" href="#">Sign Up</a></li>
                      <li class="dropdown">
                         <a href="http://www.jquery2dotnet.com" class="dropdown-toggle" data-toggle="dropdown">Sign in <b class="caret"></b></a>
                         <ul class="dropdown-menu" style="padding: 15px;min-width: 250px;">
                            <li>
                               <div class="row">
                                  <div class="col-md-12">
                                     <form class="form" ng-click="$event.stopPropagation();" ng-submit="login()" id="login-nav">
                                        <div class="form-group">
                                           <label class="sr-only" for="email">Email address</label>
                                           <input type="email" class="form-control" ng-model="users.email" id="email" placeholder="Email address" required>
                                        </div>
                                        <div class="form-group">
                                           <label class="sr-only" for="password">Password</label>
                                           <input type="password" class="form-control" ng-model="users.password" id="password" placeholder="Password" required>
                                        </div>                                        
                                        <div class="form-group">
                                           <button type="submit" class="btn btn-success btn-block">Sign in</button>
                                        </div>
                                     </form>
                                  </div>
                               </div>
                            </li>                            
                         </ul>
                      </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right" ng-show="isLogin" class="ng-show">                      
                      <li ui-sref-active="active"><a href="#" ui-sref="profile">{{profile.fullname}}</a></li>
                      <li><a href="#" ui-sref="home" ng-click="logout()">Logout</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <div class="app" id="app" ui-view></div>
    </div>
    <!-- /.container -->
    <!-- Footer -->
    <footer id="footer">
       <div class="container" role="contentinfo">
        <div class="row">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-md-12">
                Copyright @easyservise.All right reserved
              </div>
            </div>
          </div>
        </div><!--/row-->
      </div>
        <!-- /.row -->
    </footer>
        <!-- jQuery -->
        <script src="<?php echo asset('ng-libs/jquery/jquery.min.js');?>"></script>
        <!-- Angular -->
        <script src="<?php echo asset('ng-libs/angular/angular.min.js');?>"></script>
        <script src="<?php echo asset('ng-libs/angular/angular-cookies.min.js');?>"></script>
        <script src="<?php echo asset('ng-libs/angular/angular-animate.min.js');?>"></script>
        <script src="<?php echo asset('ng-libs/angular/angular-ui-router.min.js');?>"></script>
        <script src="<?php echo asset('ng-libs/angular/angular-translate.js');?>"></script>
        <script src="<?php echo asset('ng-libs/angular/angular-resource.min.js');?>"></script>
        <script src="<?php echo asset('ng-libs/angular/ngStorage.min.js');?>"></script>
        <script src="<?php echo asset('ng-libs/angular/ui-load.js');?>"></script>
        <script src="<?php echo asset('ng-libs/angular/ui-jq.js');?>"></script>
        <!--<script src="<?php echo asset('ng-libs/angular/ui-validate.js');?>"></script>-->
        <script src="<?php echo asset('ng-libs/libs/jcs-auto-validate.min.js');?>"></script>
        <script src="<?php echo asset('ng-libs/angular/ui-bootstrap-tpls.min.js');?>"></script>
        <script src="<?php echo asset('ng-libs/modules/ng-loading-bar/loading-bar.min.js');?>"></script>
        <script src="<?php echo asset('ng-libs/modules/ng-notification/angular-ui-notification.min.js');?>"></script>
        <!-- App -->
        <script src="<?php echo asset('app/frontend/app.js');?>"></script>
        <script src="<?php echo asset('app/frontend/services/services.js');?>"></script>
        <script src="<?php echo asset('app/frontend/controllers/controllers.js');?>"></script>
        <script src="<?php echo asset('app/frontend/filters/filters.js');?>"></script>
        <script src="<?php echo asset('app/frontend/directives/directives.js');?>"></script>
        <script src="<?php echo asset('app/frontend/controllers/home.js');?>"></script>
        <!-- Lazy loading -->
      
    </body>
</html>