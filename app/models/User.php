<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;

class User extends Eloquent implements UserInterface {
    use UserTrait;
    protected $table = 'users';

    protected $hidden = array('password');

    public function skill()
    {
        return $this->belongsTo('Skill', 'skill');
    }

    public function scopeUserSkill($query,$id) {
        return $query->where('skill','=',$id);
    }

    public function scopeSearch($query,$string) {
        return $query->where('fullname', 'LIKE', "%$string%")
                     ->orWhere('email', 'like', "%$string%");
    }

}
