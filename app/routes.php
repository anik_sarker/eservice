<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('index');
    
});

Route::get('/es-admin', function()
{
    return View::make('admin');
    
});



Route::post('api/v1/auth', function(){
    $credentials = Input::only('email', 'password');
    try {
        // attempt to verify the credentials and create a token for the user
        if (! $token = JWTAuth::attempt($credentials)) {
            return Response::json(array(
                "apiVersion"=> "1.0",
                'error' => array(
                    'code' => 422,
                    'message' => 'Invalid Credentials',
                ))
            );
        }
    } catch (\JWTException $e) {
        // something went wrong whilst attempting to encode the token
        return Response::json(array(
                "apiVersion"=> "1.0",
                'error' => array(
                    'code' => 417,
                    'message' => 'Could not create the Token',
                )));
    }
    $data = array(                 
        'token' => $token
    );
    // all good so return the token
    return Response::json(compact('data'));
});

Route::get('api/v1/check', ['before' => 'jwt-auth', function () {       
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);

        unset($user['password']);
        $data = array(                 
            'user' => $user
        );
        return Response::json(compact('data'));
    }
]);

Route::group(array('prefix' => 'api/v1', 'before'=>'jwt-auth'), function()
{
    Route::resource('users', 'UserController');
    Route::resource('skills', 'SkillController');
});

Route::group(array('prefix' => 'api/v1'), function()
{
    Route::resource('users', 'UserController', ['only' => ['index', 'show','store']]);
    Route::resource('skills', 'SkillController', ['only' => ['index', 'show']]);
});

if (Config::get('database.log', false))
{           
    Event::listen('illuminate.query', function($query, $bindings, $time, $name)
    {
        $data = compact('bindings', 'time', 'name');

        // Format binding data for sql insertion
        foreach ($bindings as $i => $binding)
        {   
            if ($binding instanceof \DateTime)
            {   
                $bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
            }
            else if (is_string($binding))
            {   
                $bindings[$i] = "'$binding'";
            }   
        }       

        // Insert bindings into query
        $query = str_replace(array('%', '?'), array('%%', '%s'), $query);
        $query = vsprintf($query, $bindings); 

        Log::info($query, $data);
    });

    Event::listen('404', function()
    {
        $error = "404: " . URL::full();
        Log::error($error);
        $update = new Log();
        $update->error = $error;
        $update->update;
        return Response::error('404');
    });

    Event::listen('500', function()
    {
        Log::error($error);
        $update = new Log();
        $update->error = $error;
        $update->update;
        return Response::error('500');
    });
}