'use strict';


// Declare app level module which depends on filters, and services
var app = angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngStorage',
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ui.jq',
    'ui.validate',
    'pascalprecht.translate',
    'angular-loading-bar',
    'ui-notification',
    'app.filters',
    'app.services',
    'app.directives',
    'app.BasicControllers',
    'app.dashboardcontrollers',
    'app.SkillControllers',
    'app.UserControllers',
    //'app.AdminControllers',
  ])
.run(
  [          '$rootScope', '$state', '$stateParams',
    function ($rootScope,   $state,   $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.date = new Date();

        /*var authPage = ['signin','forgotpassword'];
        $rootScope.$on('$stateChangeStart', function (event, toState) {
            if((authPage.indexOf(toState.name) < 0) && $localStorage.isLogin==false){
                $location.path('/access/signin');
            }
        });*/
    }
  ]
)
.config(
  [          '$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
    function ($stateProvider,   $urlRouterProvider,   $controllerProvider,   $compileProvider,   $filterProvider,   $provide) {

        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.filter     = $filterProvider.register;
        app.factory    = $provide.factory;
        app.service    = $provide.service;
        app.constant   = $provide.constant;
        app.value      = $provide.value;
        var templateDir = "app/admin/templates";

        $urlRouterProvider
            .otherwise('/admin/dashboard');
        $stateProvider
            .state('admin', {
                abstract: true,
                url: '/admin',
                templateUrl: templateDir+'/layout.html',
                controller: function($scope, $state, $sessionStorage){
                    //console.log($sessionStorage);
                   if($sessionStorage.isLogin!=true)
                       return $state.go('access.signin');
               }
            })
            .state('admin.dashboard', {
                url: '/dashboard',
                templateUrl: templateDir+'/dashboard/dashboard.html'
            })

            .state('admin.users', {
                url: '/users',
                template: '<div ui-view class="fade-in-right-big smooth"></div>'
            })
            .state('admin.users.list', {
                url: '/list',
                templateUrl: templateDir+'/users/index.html'
            })
            .state('admin.users.detail', {
                url: '/detail/:id',
                templateUrl: templateDir+'/users/detail.html'
            })
            .state('admin.users.create', {
                url: '/create',
                templateUrl: templateDir+'/users/create.html'
            })
            .state('admin.users.update', {
                url: '/update/:id',
                templateUrl: templateDir+'/users/update.html'
            })

            .state('admin.skills', {
                url: '/skills',
                template: '<div ui-view class="fade-in-right-big smooth"></div>'
            })
            .state('admin.skills.list', {
                url: '/list',
                templateUrl: templateDir+'/skills/index.html'
            })
            .state('admin.skills.detail', {
                url: '/detail/:id',
                templateUrl: templateDir+'/skills/detail.html'
            })
            .state('admin.skills.create', {
                url: '/create',
                templateUrl: templateDir+'/skills/create.html'
            })
            .state('admin.skills.update', {
                url: '/update:id',
                templateUrl: templateDir+'/skills/update.html'
            })

            // others
            .state('lockme', {
                url: '/lockme',
                templateUrl: 'tpl/page_lockme.html'
            })
            .state('access', {
                url: '/access',
                template: '<div ui-view class="fade-in-right-big smooth"></div>'
            })
            .state('access.signin', {
                url: '/signin',
                templateUrl: templateDir+'/others/signin.html'
            })
            .state('access.forgotpassword', {
                url: '/forgotpassword',
                templateUrl: templateDir+'/others/forgotpassword.html'
            })
            .state('access.404', {
                url: '/404',
                templateUrl: templateDir+'/page_404.html'
            })
    }
  ]
)

.config(['$translateProvider', function($translateProvider){

  // Register a loader for the static files
  // So, the module will search missing translation tables under the specified urls.
  // Those urls are [prefix][langKey][suffix].
  $translateProvider.useStaticFilesLoader({
    prefix: 'ng-libs/l10n/',
    suffix: '.json'
  });

  // Tell the module what language to use by default
  $translateProvider.preferredLanguage('en');

  // Tell the module to store the language in the local storage
  $translateProvider.useLocalStorage();

}])

/**
 * jQuery plugin config use ui-jq directive , config the js and css files that required
 * key: function name of the jQuery plugin
 * value: array of the css js file located
 */
.constant('JQ_CONFIG', {
    easyPieChart:   ['ng-libs/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
    sparkline:      ['ng-libs/jquery/charts/sparkline/jquery.sparkline.min.js'],
    plot:           ['ng-libs/jquery/charts/flot/jquery.flot.min.js',
                        'ng-libs/jquery/charts/flot/jquery.flot.resize.js',
                        'ng-libs/jquery/charts/flot/jquery.flot.tooltip.min.js',
                        'ng-libs/jquery/charts/flot/jquery.flot.spline.js',
                        'ng-libs/jquery/charts/flot/jquery.flot.orderBars.js',
                        'ng-libs/jquery/charts/flot/jquery.flot.pie.min.js'],
    slimScroll:     ['ng-libs/jquery/slimscroll/jquery.slimscroll.min.js'],
    sortable:       ['ng-libs/jquery/sortable/jquery.sortable.js'],
    nestable:       ['ng-libs/jquery/nestable/jquery.nestable.js',
                        'ng-libs/jquery/nestable/nestable.css'],
    filestyle:      ['ng-libs/jquery/file/bootstrap-filestyle.min.js'],
    slider:         ['ng-libs/jquery/slider/bootstrap-slider.js',
                        'js/jquery/slider/slider.css'],
    chosen:         ['ng-libs/jquery/chosen/chosen.jquery.min.js',
                        'js/jquery/chosen/chosen.css'],
    TouchSpin:      ['ng-libs/jquery/spinner/jquery.bootstrap-touchspin.min.js',
                        'ng-libs/jquery/spinner/jquery.bootstrap-touchspin.css'],
    wysiwyg:        ['ng-libs/jquery/wysiwyg/bootstrap-wysiwyg.js',
                        'ng-libs/jquery/wysiwyg/jquery.hotkeys.js'],
    dataTable:      ['ng-libs/jquery/datatables/jquery.dataTables.min.js',
                        'ng-libs/jquery/datatables/dataTables.bootstrap.js',
                        'ng-libs/jquery/datatables/dataTables.bootstrap.css'],
    vectorMap:      ['ng-libs/jquery/jvectormap/jquery-jvectormap.min.js',
                        'ng-libs/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
                        'ng-libs/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
                        'ng-libs/jquery/jvectormap/jquery-jvectormap.css'],
    footable:       ['ng-libs/jquery/footable/footable.all.min.js',
                        'ng-libs/jquery/footable/footable.core.css']
    }
)
.constant('MODULE_CONFIG', {
    select2:        ['ng-libs/jquery/select2/select2.css',
                        'ng-libs/jquery/select2/select2-bootstrap.css',
                        'ng-libs/jquery/select2/select2.min.js',
                        'ng-libs/modules/ui-select2.js']
    }
)
.constant('ES_SETTINGS', {
    appName     : 'Easy Service',
    appVersion  : 1.0,
    apiUrl      : 'http://localhost/easyservice/public/api/v1'

})
;
