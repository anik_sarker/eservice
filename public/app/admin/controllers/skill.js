'use strict';

angular.module('app.SkillControllers', ['pascalprecht.translate', 'ngCookies'])
  .controller('SkillListCtrl', ['$scope', '$translate', '$localStorage', '$window', '$resource', '$log', '$modal', '$filter', 'ES_SETTINGS', 'Notification',
    function($scope,   $translate,   $localStorage,   $window, $resource, $log, $modal, $filter, ES_SETTINGS, Notification ) {
        $scope.pageHead = "Skills";
        $scope.skills = [];

        $scope.totalItems = 0;
        $scope.currentPage = 0;
        $scope.perPage = 25;
        $scope.maxSize = 5;

        var Skills = $resource(ES_SETTINGS.apiUrl+'/skills').get(function(data){            
            if(data.error==false){
                $scope.skills = data.skills.data;
                $scope.totalItems = data.skills.pagination.total;
                $scope.currentPage = data.skills.pagination.current_page;
                $scope.itemFrom = data.skills.pagination.from;
                $scope.itemTo = data.skills.pagination.to;
                $scope.perPage = data.skills.pagination.per_page;
            }
        });

        $scope.pageChanged = function() {
            var apiUrl = ES_SETTINGS.apiUrl+'/skills?page='+$scope.currentPage+'&limit='+$scope.perPage;
            if($scope.userSearch){
                apiUrl += '&search='+$scope.userSearch;
            }
            var Users = $resource(apiUrl).get(function(data){            
                if(data.error==false){
                    $scope.skills = data.skills.data;
                    $scope.totalItems = data.skills.pagination.total;
                    $scope.currentPage = data.skills.pagination.current_page;
                    $scope.itemFrom = data.skills.pagination.from;
                    $scope.itemTo = data.skills.pagination.to;
                    $scope.perPage = data.skills.pagination.per_page;
                }
            });
        };

        $scope.pageLimitChange = function() {
            var apiUrl = ES_SETTINGS.apiUrl+'/skills?limit='+$scope.perPage;
            if($scope.userSearch){
                apiUrl += '&search='+$scope.userSearch;
            }
            var Users = $resource(apiUrl).get(function(data){            
                if(data.error==false){
                    $scope.skills = data.skills.data;
                    $scope.totalItems = data.skills.pagination.total;
                    $scope.currentPage = data.skills.pagination.current_page;
                    $scope.itemFrom = data.skills.pagination.from;
                    $scope.itemTo = data.skills.pagination.to;
                    $scope.perPage = data.skills.pagination.per_page;
                }
            });
        };

        $scope.getSearch = function(){
            var apiUrl = ES_SETTINGS.apiUrl+'/skills?limit='+$scope.perPage;
            if($scope.userSearch){
                apiUrl += '&search='+$scope.userSearch;
            }
            
            var Users = $resource(apiUrl).get(function(data){            
                if(data.error==false){
                    $scope.skills = data.skills.data;
                    $scope.totalItems = data.skills.pagination.total;
                    $scope.currentPage = data.skills.pagination.current_page;
                    $scope.itemFrom = data.skills.pagination.from;
                    $scope.itemTo = data.skills.pagination.to;
                    $scope.perPage = data.skills.pagination.per_page;
                }
            });  
        };

        var ModalUserInstanceCtrl = function ($scope, $modalInstance, $filter, skill, skills, Notification) {                
            $scope.skill = skill;
            $scope.ok = function () {                    
                
                var Skills = $resource(ES_SETTINGS.apiUrl+'/skills/'+skill.id).delete(function(data){
                    if(data.error==false){
                        console.log(data);
                        Notification.success({
                            title : 'Success',
                            message : 'Data removed successfully'
                        });
                    }
                });
                skills = $filter('filter')(skills, {id: '!'+skill.id}, true);
                $modalInstance.close(skills);
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        };

        $scope.removeConfirm = function (skill) {
            $scope.skill = skill;
            var modalInstance = $modal.open({
                templateUrl: 'removeSkill.html',
                controller: ModalUserInstanceCtrl,                    
                resolve: {
                    skill: function() {
                        return $scope.skill;
                    },
                    skills : function() {
                        return $scope.skills;
                    }
                }
            });

            modalInstance.result.then(function(skills) {
                $scope.skills = skills;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
  }])

  .controller('SkillDetailCtrl', ['$scope', '$translate', '$localStorage', '$window', '$resource',  '$stateParams', 'ES_SETTINGS', 'Notification',
    function($scope,   $translate,   $localStorage,   $window, $resource, $stateParams, ES_SETTINGS, Notification ) {
      $scope.pageHead = "Skills";
      $scope.skills = [];
      var Skills = $resource(ES_SETTINGS.apiUrl+'/skills/'+$stateParams.id).get(function(data){            
        if(data.error==false){
            $scope.skills = data.skills;
        }
      });
  }])

  .controller('SkillUpdateCtrl', ['$scope', '$translate', '$sessionStorage', '$window', '$resource', '$stateParams', '$http', '$log', '$modal', '$filter', 'ES_SETTINGS', 'Notification',
        function($scope,   $translate,   $sessionStorage,   $window, $resource, $stateParams, $http, $log, $modal, $filter, ES_SETTINGS, Notification ) {
        $scope.pageHead = "Skills";

        $scope.skills = [];
        var Skills = $resource(ES_SETTINGS.apiUrl+'/skills/'+$stateParams.id).get(function(data){            
            if(data.error==false){
                $scope.skills = data.skills;
            }
        });

        $scope.submitForm = function (){
            console.log($scope.skills);

            $http.defaults.headers.common.Authorization = 'Bearer '+ $sessionStorage.token;
            $http.put(ES_SETTINGS.apiUrl+'/skills/'+$stateParams.id, $scope.skills).
                success(function(data, status, headers, config) {
                    console.log(data);
                    Notification.success({
                        title : 'Success',
                        message : data.response
                    });
                    $scope.skills = data.skills;
                }).
                error(function(data, status, headers, config) {
                    Notification.error({
                        title : 'Error!',
                        message : data.response
                    });
                });
        };
        
    }])
  .controller('SkillInsertCtrl', ['$scope', '$translate', '$sessionStorage', '$window', '$resource', '$stateParams', '$http', '$log', '$modal', '$filter','Notification', 'ES_SETTINGS',
        function($scope,   $translate,   $sessionStorage,   $window, $resource, $stateParams, $http, $log, $modal, $filter, Notification, ES_SETTINGS ) {
        $scope.pageHead = "Skills";

        $scope.skills = [];        

        $scope.submitForm = function (){
            $log.info($scope.skills);
            //var skill = $scope.skills.name
            $resource(ES_SETTINGS.apiUrl+'/skills').save({name : $scope.skills.name},function(data){
                if(data.error==false){
                    Notification.success({
                        title : 'Success',
                        message : data.response
                    });
                    console.log(data);
                } else {
                    Notification.error({
                        title : 'Error',
                        message : data.response
                    });
                }
            });
        };
        
    }])