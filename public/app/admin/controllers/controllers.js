'use strict';

/* Controllers */

angular.module('app.BasicControllers', ['pascalprecht.translate', 'ngCookies'])
  .controller('AppCtrl', ['$scope', '$translate', '$localStorage', '$window', '$state', '$sessionStorage',
      function( $scope, $translate, $localStorage, $window, $state, $sessionStorage ) {
        // add 'ie' classes to html
        var isIE = !!navigator.userAgent.match(/MSIE/i);
        isIE && angular.element($window.document.body).addClass('ie');
        isSmartDevice( $window ) && angular.element($window.document.body).addClass('smart');

      // config
        $scope.app = {
            name : 'EasyService',
            version : '1.0',
            apiURL : '/api/v1/',
            // for chart colors
            color: {
              primary: '#7266ba',
              info:    '#23b7e5',
              success: '#27c24c',
              warning: '#fad733',
              danger:  '#f05050',
              light:   '#e8eff0',
              dark:    '#3a3f51',
              black:   '#1c2b36'
            },
            settings: {
              themeID: 1,
              navbarHeaderColor: 'bg-black',
              navbarCollapseColor: 'bg-white-only',
              asideColor: 'bg-black',
              headerFixed: true,
              asideFixed: true,
              asideFolded: false
            }
        }

          // save settings to local storage
          if ( angular.isDefined($localStorage.settings) ) {
            $scope.app.settings = $localStorage.settings;
          } else {
            $localStorage.settings = $scope.app.settings;
          }

          if ( angular.isDefined($sessionStorage.isLogin) ) {
              $scope.isLogin = $sessionStorage.isLogin;
              $scope.profile = $sessionStorage.profile;
          } else {
              $sessionStorage.isLogin = false;
              $scope.profile = false;
          }
          $scope.$watch('app.settings', function(){ $localStorage.settings = $scope.app.settings; }, true);

          // angular translate
          $scope.lang = { isopen: false };
          $scope.langs = {en:'English', de_DE:'German', it_IT:'Italian'};
          $scope.selectLang = $scope.langs[$translate.proposedLanguage()] || "English";
          $scope.setLang = function(langKey, $event) {
            // set the current lang
            $scope.selectLang = $scope.langs[langKey];
            // You can change the language during runtime
            $translate.use(langKey);
            $scope.lang.isopen = !$scope.lang.isopen;
          };

          function isSmartDevice( $window )
          {
              // Adapted from http://www.detectmobilebrowsers.com
              var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
              // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
              return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
          }

          $scope.logout = function() {
            delete $sessionStorage.profile;
            delete $sessionStorage.token;
            delete $sessionStorage.isLogin;
            delete $sessionStorage.userID;
            $scope.isLogin = false;
            $state.go('access.signin');
          };

  }])

  // signin controller
  .controller('LoginController', ['$scope', '$http', '$resource', '$state', '$sessionStorage', 'Notification', 'ES_SETTINGS',
      function($scope, $http, $resource, $state, $sessionStorage, Notification, ES_SETTINGS) {
        $scope.user = {};
        $scope.authError = null;

        $scope.login = function() {
            $scope.authError = null;
            var loginUser = {
              email   : $scope.user.email,
              password: $scope.user.password
            }

            // Try to login
            var Login = $resource(ES_SETTINGS.apiUrl+'/auth').save(loginUser, function(reply){

              delete $sessionStorage.profile;
              delete $sessionStorage.token;
              delete $sessionStorage.isLogin;
              delete $sessionStorage.userID;
              $scope.isLogin = false;

              if(reply.data) {
                $sessionStorage.token = reply.data.token;
                $sessionStorage.isLogin = true;
                $scope.isLogin = $sessionStorage.isLogin;

                $http.defaults.headers.common.Authorization = 'Bearer '+ $sessionStorage.token;
                $resource(ES_SETTINGS.apiUrl+'/check').get(function(reply){
                  console.log(reply.data.user);
                  if(reply.data){
                    if(reply.data.user.is_admin == '1'){
                      Notification.success({
                        title : 'Success',
                        message : 'Login success. please wait...'
                      });
                      $sessionStorage.userID = reply.data.user.id;
                      $sessionStorage.isLogin = true;
                        $sessionStorage.profile = reply.data.user;
                      $state.go('admin.dashboard');
                    } else {
                      delete $sessionStorage.profile;
                      delete $sessionStorage.token;
                      delete $sessionStorage.isLogin;
                      delete $sessionStorage.userID;
                      $scope.isLogin = false;

                      Notification.warning({
                        title : 'Warning',
                        message : 'You are not authorize user'
                      });
                      $scope.authError = 'Sorry, You are not authorize user';
                    }
                  } else {
                    Notification.error({
                      title : 'Error',
                      message : reply.error
                    });
                  }
                });
                //setTimeout(function(){$state.go('profile');},1000);
              } else {
                Notification.error({
                  title : 'Error',
                  message : reply.error.message
                });
                $scope.authError = reply.error.message;
              }
            });
        };
  }])

  // signup controller
  .controller('SignupFormController', ['$scope', '$http', '$state', function($scope, $http, $state) {
    $scope.user = {};
    $scope.authError = null;
    $scope.signup = function() {
      $scope.authError = null;
      // Try to create
      $http.post('api/signup', {name: $scope.user.name, email: $scope.user.email, password: $scope.user.password})
      .then(function(response) {
        if ( !response.data.user ) {
          $scope.authError = response;
        }else{
          $state.go('admin.dashboard');
        }
      }, function(x) {
        $scope.authError = 'Server Error';
      });
    };
  }])
  ;