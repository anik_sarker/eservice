'use strict';

angular.module('app.UserControllers', ['pascalprecht.translate', 'ngCookies'])
    .controller('UserListCtrl', ['$scope', '$translate', '$sessionStorage', '$window', '$resource', '$log', '$modal', '$filter', 'ES_SETTINGS', 'Notification',
        function($scope,   $translate,   $sessionStorage,   $window, $resource, $log, $modal, $filter, ES_SETTINGS, Notification ) {
            $scope.pageHead = "Users";
            $scope.users = [];

            $scope.totalItems = 0;
            $scope.currentPage = 0;
            $scope.perPage = 25;
            $scope.maxSize = 5;

            $scope.pageChanged = function() {
                var apiUrl = ES_SETTINGS.apiUrl+'/users?page='+$scope.currentPage+'&limit='+$scope.perPage;
                if($scope.userSearch){
                    apiUrl += '&search='+$scope.userSearch;
                }
                var Users = $resource(apiUrl).get(function(data){            
                    if(data.error==false){
                        $scope.users = data.users.data;
                        $scope.totalItems = data.users.pagination.total;
                        $scope.currentPage = data.users.pagination.current_page;
                        $scope.itemFrom = data.users.pagination.from;
                        $scope.itemTo = data.users.pagination.to;
                        $scope.perPage = data.users.pagination.per_page;
                    }
                });
            };

            $scope.pageLimitChange = function() {
                var apiUrl = ES_SETTINGS.apiUrl+'/users?limit='+$scope.perPage;
                if($scope.userSearch){
                    apiUrl += '&search='+$scope.userSearch;
                }
                var Users = $resource(apiUrl).get(function(data){            
                    if(data.error==false){
                        $scope.users = data.users.data;
                        $scope.totalItems = data.users.pagination.total;
                        $scope.currentPage = data.users.pagination.current_page;
                        $scope.itemFrom = data.users.pagination.from;
                        $scope.itemTo = data.users.pagination.to;
                        $scope.perPage = data.users.pagination.per_page;
                    }
                });
            };

            $scope.getSearch = function(){
                var apiUrl = ES_SETTINGS.apiUrl+'/users?limit='+$scope.perPage;
                if($scope.userSearch){
                    apiUrl += '&search='+$scope.userSearch;
                }
                
                var Users = $resource(apiUrl).get(function(data){            
                    if(data.error==false){
                        $scope.users = data.users.data;
                        $scope.totalItems = data.users.pagination.total;
                        $scope.currentPage = data.users.pagination.current_page;
                        $scope.itemFrom = data.users.pagination.from;
                        $scope.itemTo = data.users.pagination.to;
                        $scope.perPage = data.users.pagination.per_page;
                    }
                });  
            };

            var ModalUserInstanceCtrl = function ($scope, $modalInstance, $filter, user, users, Notification) {                
                $scope.user = user;
                $scope.ok = function () {                    
                    
                    var Users = $resource(ES_SETTINGS.apiUrl+'/users/'+user.id).delete(function(data){
                        if(data.error==false){
                            console.log(data);
                            Notification.success({
                                title : 'Success',
                                message : 'Data removed successfully'
                            });
                        }
                    });
                    users = $filter('filter')(users, {id: '!'+user.id}, true);
                    $modalInstance.close(users);
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            };

            $scope.removeConfirm = function (user) {
                $scope.user = user;
                var modalInstance = $modal.open({
                    templateUrl: 'removeUser.html',
                    controller: ModalUserInstanceCtrl,                    
                    resolve: {
                        user: function() {
                            return $scope.user;
                        },
                        users : function() {
                            return $scope.users;
                        }
                    }
                });

                modalInstance.result.then(function(users) {
                    $scope.users = users;
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };
            var Users = $resource(ES_SETTINGS.apiUrl+'/users');
            Users.get(function(data){            
                if(data.error==false){
                    $scope.users = data.users.data;
                    $scope.totalItems = data.users.pagination.total;
                    $scope.currentPage = data.users.pagination.current_page;
                    $scope.itemFrom = data.users.pagination.from;
                    $scope.itemTo = data.users.pagination.to;
                    $scope.perPage = data.users.pagination.per_page;
                }
            });

    }])
    .controller('UserDetailCtrl', ['$scope', '$translate', '$sessionStorage', '$window', '$resource', '$stateParams',  '$filter', 'ES_SETTINGS',
        function($scope,   $translate, $sessionStorage,   $window, $resource, $stateParams, $filter, ES_SETTINGS ) {
            $scope.pageHead = "Users";

            $scope.user = [];           
            var Users = $resource(ES_SETTINGS.apiUrl+'/users/'+$stateParams.id).get(function(data){            
                if(data.error==false){
                    $scope.user = data.users;                    
                }
            });
    }])
    .controller('UserUpdateCtrl', ['$scope', '$translate', '$sessionStorage', '$window', '$resource', '$stateParams', '$http', '$log', '$modal', '$filter', 'ES_SETTINGS', 'Notification', 'fileReader',
        function($scope,   $translate,   $sessionStorage,   $window, $resource, $stateParams, $http, $log, $modal, $filter, ES_SETTINGS, Notification, fileReader ) {
        $scope.pageHead = "Users";

        $scope.user  = [];
        $scope.skills = [];
        
        var Users = $resource(ES_SETTINGS.apiUrl+'/users/'+$stateParams.id);
        
        Users.get(function(data){            
            if(data.error==false){
                $scope.user = data.users;                
            }
        });

        var Skills = $resource(ES_SETTINGS.apiUrl+'/skills?limit=all');
        Skills.get(function(data){
            if(data.error==false){
                $scope.skills = data.skills.data;               
            }
        });

        $scope.submitForm = function (){
            var formUser = {
                fullname            : $scope.user.fullname,
                skill               : $scope.user.skill,
                phone               : $scope.user.phone,
                birthdate           : $scope.user.birthdate,
                gender              : $scope.user.gender,
                address             : $scope.user.address,
                aboutme             : $scope.user.aboutme,
                photo               : $scope.user.photo
            };
            $http.defaults.headers.common.Authorization = 'Bearer '+ $sessionStorage.token;
            $http.put(ES_SETTINGS.apiUrl+'/users/'+$stateParams.id, formUser).
                success(function(data, status, headers, config) {
                    if(data.error==false){
                        Notification.success({
                            title : 'Success',
                            message : data.response
                        });
                    } else {
                        Notification.error({
                            title : 'Error!',
                            message : data.response
                        });
                    }
                }).
                error(function(data, status, headers, config) {
                
                });
        };

        var ModalPasswordInstanceCtrl = function ($scope, $modalInstance, $filter, user) {                
            $scope.user = user;
            $scope.ok = function() {
                Notification.success({
                    title : 'Success',
                    message : "Password Change"
                });
                $modalInstance.close(user);
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        };

        $scope.changePassword = function (user) {
            $scope.user = user;
            var modalInstance = $modal.open({
                templateUrl: 'passwordForm.html',
                controller: ModalPasswordInstanceCtrl,                    
                resolve: {
                    user: function() {
                        return $scope.user;
                    }
                }
            });

            modalInstance.result.then(function(users) {
                $scope.users = users;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.getFile = function () {
            $scope.progress = 0;
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function(result) {
                    $scope.user.photo = result;
                    //$log.info($scope.user.photo)
                });
        };

        $scope.$on("fileProgress", function(e, progress) {
            $scope.progress = progress.loaded / progress.total;
        });

    }])