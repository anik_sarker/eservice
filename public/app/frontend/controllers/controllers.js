'use strict';

/* Controllers */

angular.module('app.BasicControllers', ['pascalprecht.translate', 'ngCookies'])
  .controller('AppCtrl', ['$scope','$http' , '$state', '$translate', '$resource', '$localStorage', '$window', '$log', 'ES_SETTINGS', 'Notification',
    function( $scope, $http, $state, $translate, $resource, $localStorage,   $window, $log, ES_SETTINGS, Notification ) {
      // add 'ie' classes to html
      var isIE = !!navigator.userAgent.match(/MSIE/i);
      isIE && angular.element($window.document.body).addClass('ie');
      isSmartDevice( $window ) && angular.element($window.document.body).addClass('smart');

      // config
      $scope.app = {
        name : 'EasyService',
        version : '1.0',
        apiURL : '/api/v1/',
      }

      // save settings to local storage
      if ( angular.isDefined($localStorage.settings) ) {
        $scope.app.settings = $localStorage.settings;
      } else {
        $localStorage.settings = $scope.app.settings;
      }

      if ( angular.isDefined($localStorage.isLogin) ) {
        $scope.isLogin = $localStorage.isLogin;
        $scope.profile = $localStorage.profile;
      } else {
        $localStorage.isLogin = false;
        $scope.profile = false;
      }

      $scope.$watch('app.settings', function(){ $localStorage.settings = $scope.app.settings; }, true);

      // angular translate
      $scope.lang = { isopen: false };
      $scope.langs = {en:'English', de_DE:'German', it_IT:'Italian'};
      $scope.selectLang = $scope.langs[$translate.proposedLanguage()] || "English";
      $scope.setLang = function(langKey, $event) {
        // set the current lang
        $scope.selectLang = $scope.langs[langKey];
        // You can change the language during runtime
        $translate.use(langKey);
        $scope.lang.isopen = !$scope.lang.isopen;
      };

      function isSmartDevice( $window )
      {
          // Adapted from http://www.detectmobilebrowsers.com
          var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
          // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
          return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
      }

      $scope.isCollapsed = true;

      $scope.login = function(){
        $log.info($scope.users);
        var loginUser = {
            email     : $scope.users.email,
            password  : $scope.users.password
        };

        var Login = $resource(ES_SETTINGS.apiUrl+'/auth').save(loginUser, function(reply){

            delete $localStorage.profile;
            delete $localStorage.token;
            delete $localStorage.isLogin;
            delete $localStorage.userID;
            $scope.isLogin = false;

            if(reply.data) {
              Notification.success({
                    title : 'Success',
                    message : 'Login success. please wait...'
              });
              $localStorage.token = reply.data.token;
              $localStorage.isLogin = true;
              $scope.isLogin = $localStorage.isLogin;

              $http.defaults.headers.common.Authorization = 'Bearer '+ $localStorage.token;

              $resource(ES_SETTINGS.apiUrl+'/check').get(function(reply){
                if(reply.data){
                    $localStorage.userID = reply.data.user.id;
                    $localStorage.isLogin = true;
                    $state.go('profile');
                } else {
                  Notification.error({
                    title : 'Error',
                    message : reply.error
                  });
                }
              });
              //setTimeout(function(){$state.go('profile');},1000);
            } else {
                Notification.error({
                    title : 'Error',
                    message : reply.error.message
                });
            }
        });
      };

      $scope.logout = function() {
        delete $localStorage.profile;
        delete $localStorage.token;
        delete $localStorage.isLogin;
        delete $localStorage.userID;
        $scope.isLogin = false;
        $state.go('home');
      };
  }])

  ;
