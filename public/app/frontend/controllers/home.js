'use strict';

/* Controllers */

angular.module('app.HomeControllers', ['pascalprecht.translate', 'ngCookies'])
  .controller('HomeCtrl', ['$scope', '$translate', '$localStorage', '$window', '$resource', '$log', '$modal', '$filter', 'ES_SETTINGS',
    function( $scope,   $translate,   $localStorage,   $window, $resource, $log, $modal, $filter, ES_SETTINGS ) {
        $scope.pageHead = "Users";
        $scope.isCollapsed = true;
        $scope.selectedSkill = "All";
        $scope.selectSkillID = "all";
        $scope.users = [];

        $scope.totalItems = 0;
        $scope.currentPage = 0;
        $scope.perPage = 25;
        $scope.maxSize = 5;

        $scope.pageChanged = function() {
            var apiUrl = ES_SETTINGS.apiUrl+'/users?page='+$scope.currentPage+'&limit='+$scope.perPage;
            if($scope.userSearch){
                apiUrl += '&search='+$scope.userSearch;
            }
            if($scope.selectSkillID){
                apiUrl += '&skill='+$scope.selectSkillID;
            }
            var Users = $resource(apiUrl).get(function(data){
                if(data.error==false){
                    $scope.users = data.users.data;
                    $scope.totalItems = data.users.pagination.total;
                    $scope.currentPage = data.users.pagination.current_page;
                    $scope.itemFrom = data.users.pagination.from;
                    $scope.itemTo = data.users.pagination.to;
                    $scope.perPage = data.users.pagination.per_page;
                }
            });
        };

        $scope.pageLimitChange = function() {
            var apiUrl = ES_SETTINGS.apiUrl+'/users?limit='+$scope.perPage;
            if($scope.userSearch){
                apiUrl += '&search='+$scope.userSearch;
            }
            if($scope.selectSkillID){
                apiUrl += '&skill='+$scope.selectSkillID;
            }
            var Users = $resource(apiUrl).get(function(data){
                if(data.error==false){
                    $scope.users = data.users.data;
                    $scope.totalItems = data.users.pagination.total;
                    $scope.currentPage = data.users.pagination.current_page;
                    $scope.itemFrom = data.users.pagination.from;
                    $scope.itemTo = data.users.pagination.to;
                    $scope.perPage = data.users.pagination.per_page;
                }
            });
        };

        $scope.getSearch = function(){
            var apiUrl = ES_SETTINGS.apiUrl+'/users?limit='+$scope.perPage;
            if($scope.userSearch){
                apiUrl += '&search='+$scope.userSearch;
            }
            if($scope.skillSearch){
                apiUrl += '&skill='+$scope.selectSkillID;
            }

            var Users = $resource(apiUrl).get(function(data){
                if(data.error==false){
                    $scope.users = data.users.data;
                    $scope.totalItems = data.users.pagination.total;
                    $scope.currentPage = data.users.pagination.current_page;
                    $scope.itemFrom = data.users.pagination.from;
                    $scope.itemTo = data.users.pagination.to;
                    $scope.perPage = data.users.pagination.per_page;
                }
            });
        };

        $scope.changeSkill = function(skillID, skillName){
            $scope.selectedSkill = skillName;
            $scope.selectSkillID = skillID;
            var Users = $resource(ES_SETTINGS.apiUrl+'/users?limit=24&skill='+$scope.selectSkillID).get(function(data){
                if(data.error==false){
                    $scope.users = data.users.data;
                    $scope.totalItems = data.users.pagination.total;
                    $scope.currentPage = data.users.pagination.current_page;
                    $scope.itemFrom = data.users.pagination.from;
                    $scope.itemTo = data.users.pagination.to;
                    $scope.perPage = data.users.pagination.per_page;
                }
            });

        };

        var Users = $resource(ES_SETTINGS.apiUrl+'/users?limit=24').get(function(data){
            if(data.error==false){
                $scope.users = data.users.data;
                $scope.totalItems = data.users.pagination.total;
                $scope.currentPage = data.users.pagination.current_page;
                $scope.itemFrom = data.users.pagination.from;
                $scope.itemTo = data.users.pagination.to;
                $scope.perPage = data.users.pagination.per_page;
            }
        });

        var Skills = $resource(ES_SETTINGS.apiUrl+'/skills?limit=all').get(function(data){
            if(data.error==false){
                $scope.skills = data.skills.data;
            }
        });

        var ModalUserInstanceCtrl = function ($scope, $modalInstance) {
            $scope.ok = function () {
                $modalInstance.dismiss('cancel');
            };
        };

        $scope.userDetails = function (user) {
            $scope.selectedUser = user;
            var modalInstance = $modal.open({
                templateUrl: 'detailUser.html',
                controller: ModalUserInstanceCtrl,
                scope : $scope
            });
        };

  }])
  .controller('RegistrationCtrl', ['$scope', '$translate', '$localStorage', '$window', '$resource', '$log', '$modal', '$filter', 'ES_SETTINGS', 'Notification',
    function( $scope,   $translate,   $localStorage,   $window, $resource, $log, $modal, $filter, ES_SETTINGS, Notification ) {
        var Skills = $resource(ES_SETTINGS.apiUrl+'/skills?limit=all').get(function(data){
            if(data.error==false){
                $scope.skills = data.skills.data;
            }
        });
        $scope.checkUniqueEmail = true;

        $scope.signup = function(){
            $log.info($scope.user);
            var formUser = {
                fullname            : $scope.user.fullname,
                email               : $scope.user.email,
                password            : $scope.user.password,
                password_confirm    : $scope.user.password_confirm,
                skill               : $scope.user.skill,
                phone               : $scope.user.phone,
                birthdate           : $scope.user.birthdate,
                gender              : $scope.user.gender,
                address             : $scope.user.address,
                aboutme             : $scope.user.aboutme
            };
            var Users = $resource(ES_SETTINGS.apiUrl+'/users').save(formUser,function(data){
                if(data.error==false){
                    Notification.success({
                        title : 'Success',
                        message : '<strong>Congratulations.</strong> Registration process complete.'
                    });
                    $log.info(data);
                } else {
                    $log.info(data.message);
                    $scope.checkUniqueEmail = false;
                    //$scope.form.user.email.$error.unique = false;
                    Notification.error({
                        title : 'Error',
                        message : '<strong>Sorry! </strong><br />' + data.message.email[0]
                    });
                }
            });
        };
  }])
  .controller('ProfileCtrl', ['$scope', '$translate', '$localStorage', '$window', '$resource', '$log', '$modal', '$filter', 'ES_SETTINGS',
    function( $scope, $translate, $localStorage, $window, $resource, $log, $modal, $filter, ES_SETTINGS ) {
        if($localStorage.hasOwnProperty('profile')) {
            $scope.profile = $localStorage.profile;
        } else {
           $resource(ES_SETTINGS.apiUrl+'/users/'+$localStorage.userID).get(function(data){
                if(data.error===false){
                    $localStorage.profile = data.users;
                    $scope.profile = $localStorage.profile;
                }
            });
        }

  }])
  .controller('EditProfileCtrl', ['$scope','$state', '$http', '$translate', 'fileReader', '$localStorage', '$window', '$resource', '$log', '$modal', '$filter', 'ES_SETTINGS', 'Notification',
    function( $scope, $state,  $http, $translate, fileReader, $localStorage, $window, $resource, $log, $modal, $filter, ES_SETTINGS, Notification ) {
        $resource(ES_SETTINGS.apiUrl+'/skills?limit=all').get(function(data){
            if(data.error==false){
                $scope.skills = data.skills.data;
            }
        });
        if($localStorage.hasOwnProperty('profile')) {
            $scope.user = $localStorage.profile;
        } else {
            $resource(ES_SETTINGS.apiUrl+'/users/'+$localStorage.userID).get(function(data){
                if(data.error==false){
                    $scope.user = data.users;
                    $localStorage.profile = data.users;
                }
            });
        }

        $scope.getFile = function () {
            $scope.progress = 0;
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function(result) {
                    $scope.user.photo = result;
                    //$log.info($scope.user.photo)
                });
        };

        $scope.$on("fileProgress", function(e, progress) {
            $scope.progress = progress.loaded / progress.total;
        });

        var ModalPasswordInstanceCtrl = function ($scope, $modalInstance, $filter, user) {
            //$scope.user = user;
            $scope.ok = function(password) {
                $log.info(password)
                var formUser = {
                    password : password
                };

                $http.defaults.headers.common.Authorization = 'Bearer '+ $localStorage.token;
                $http.put(ES_SETTINGS.apiUrl+'/users/'+$localStorage.userID, formUser).
                    success(function(data, status, headers, config) {
                        if(data.error==false){
                            Notification.success({
                                title : 'Success',
                                message : 'Password change successfully'
                            });
                            $modalInstance.close(user);
                        }
                    }).
                    error(function(data, status, headers, config) {
                        Notification.error({
                            title : 'Error',
                            message : data.error.message
                        });
                    });
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        };

        $scope.changePassword = function (user) {
            $scope.user = user;
            var modalInstance = $modal.open({
                templateUrl: 'passwordForm.html',
                controller: ModalPasswordInstanceCtrl,
                resolve: {
                    user: function() {
                        return $scope.user;
                    }
                }
            });

            modalInstance.result.then(function(user) {
                //$scope.users = users;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.submitForm = function(){
            var formUser = {
                fullname            : $scope.user.fullname,
                skill               : $scope.user.skill,
                phone               : $scope.user.phone,
                birthdate           : $scope.user.birthdate,
                gender              : $scope.user.gender,
                address             : $scope.user.address,
                aboutme             : $scope.user.aboutme,
                photo               : $scope.user.photo
            };

            $http.defaults.headers.common.Authorization = 'Bearer '+ $localStorage.token;
            $http.put(ES_SETTINGS.apiUrl+'/users/'+$localStorage.userID, formUser).
                success(function(data, status, headers, config) {
                   if(data.error==false){
                       delete $localStorage.profile;
                       $state.go('profile');
                   }
                }).
                error(function(data, status, headers, config) {
                    Notification.error({
                        title : 'Error',
                        message : data.error.message
                    });
                });
        };

  }])
;
