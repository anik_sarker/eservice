'use strict';


// Declare app level module which depends on filters, and services
var app = angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngStorage',
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ui.jq',
    /*'ui.validate',*/
    'jcs-autoValidate',
    'pascalprecht.translate',
    'angular-loading-bar',
    'ui-notification',
    'app.filters',
    'app.services',
    'app.directives',
    'app.BasicControllers',
    'app.HomeControllers',
  ])
.run(
  [ '$rootScope', '$location', '$state', '$stateParams', '$localStorage', 'bootstrap3ElementModifier', 'defaultErrorMessageResolver',
    function ($rootScope, $location, $state, $stateParams, $localStorage, bootstrap3ElementModifier, defaultErrorMessageResolver) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        bootstrap3ElementModifier.enableValidationStateIcons(true);
        defaultErrorMessageResolver.getErrorMessages().then(function (errorMessages) {
            errorMessages['retypePassword'] = 'Password do not match';
        });
        var authPage = ['profile','edit-profile','change-password'];
        $rootScope.$on('$stateChangeStart', function (event, toState) {
            if((authPage.indexOf(toState.name) >= 0) && $localStorage.isLogin==false){
                $location.path('/home');
            }
        });

    }
  ]
)
.config(
  [          '$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
    function ($stateProvider,   $urlRouterProvider,   $controllerProvider,   $compileProvider,   $filterProvider,   $provide) {

        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.filter     = $filterProvider.register;
        app.factory    = $provide.factory;
        app.service    = $provide.service;
        app.constant   = $provide.constant;
        app.value      = $provide.value;
        var templateDir = "app/frontend/templates";

        $urlRouterProvider
            .otherwise('/home');
        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: templateDir+'/home/home.html'
            })
            .state('contact', {
                url: '/contact',
                templateUrl: templateDir+'/home/contact.html'
            })
            .state('registration', {
                url: '/registration',
                templateUrl: templateDir+'/home/registration.html'
            })
            .state('profile', {
                url: '/profile',
                templateUrl: templateDir+'/home/profile.html'
            })
            .state('edit-profile', {
                url: '/edit-profile',
                templateUrl: templateDir+'/home/edit-profile.html'
            })
            .state('logout', {
                url: '/logout'
            })
    }
  ]
)

/**
 * jQuery plugin config use ui-jq directive , config the js and css files that required
 * key: function name of the jQuery plugin
 * value: array of the css js file located
 */
.constant('JQ_CONFIG', {
    easyPieChart:   ['ng-libs/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
    sparkline:      ['ng-libs/jquery/charts/sparkline/jquery.sparkline.min.js'],
    plot:           ['ng-libs/jquery/charts/flot/jquery.flot.min.js',
                        'ng-libs/jquery/charts/flot/jquery.flot.resize.js',
                        'ng-libs/jquery/charts/flot/jquery.flot.tooltip.min.js',
                        'ng-libs/jquery/charts/flot/jquery.flot.spline.js',
                        'ng-libs/jquery/charts/flot/jquery.flot.orderBars.js',
                        'ng-libs/jquery/charts/flot/jquery.flot.pie.min.js'],
    slimScroll:     ['ng-libs/jquery/slimscroll/jquery.slimscroll.min.js'],
    sortable:       ['ng-libs/jquery/sortable/jquery.sortable.js'],
    nestable:       ['ng-libs/jquery/nestable/jquery.nestable.js',
                        'ng-libs/jquery/nestable/nestable.css'],
    filestyle:      ['ng-libs/jquery/file/bootstrap-filestyle.min.js'],
    slider:         ['ng-libs/jquery/slider/bootstrap-slider.js',
                        'js/jquery/slider/slider.css'],
    chosen:         ['ng-libs/jquery/chosen/chosen.jquery.min.js',
                        'js/jquery/chosen/chosen.css'],
    TouchSpin:      ['ng-libs/jquery/spinner/jquery.bootstrap-touchspin.min.js',
                        'ng-libs/jquery/spinner/jquery.bootstrap-touchspin.css'],
    wysiwyg:        ['ng-libs/jquery/wysiwyg/bootstrap-wysiwyg.js',
                        'ng-libs/jquery/wysiwyg/jquery.hotkeys.js'],
    dataTable:      ['ng-libs/jquery/datatables/jquery.dataTables.min.js',
                        'ng-libs/jquery/datatables/dataTables.bootstrap.js',
                        'ng-libs/jquery/datatables/dataTables.bootstrap.css'],
    vectorMap:      ['ng-libs/jquery/jvectormap/jquery-jvectormap.min.js',
                        'ng-libs/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
                        'ng-libs/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
                        'ng-libs/jquery/jvectormap/jquery-jvectormap.css'],
    footable:       ['ng-libs/jquery/footable/footable.all.min.js',
                        'ng-libs/jquery/footable/footable.core.css']
    }
)
.constant('MODULE_CONFIG', {
    select2:        ['ng-libs/jquery/select2/select2.css',
                        'ng-libs/jquery/select2/select2-bootstrap.css',
                        'ng-libs/jquery/select2/select2.min.js',
                        'ng-libs/modules/ui-select2.js']
    }
)
.constant('ES_SETTINGS', {
    appName     : 'Easy Service',
    appVersion  : 1.0,
    apiUrl      : 'http://localhost/easyservice/public/api/v1'
})
;
