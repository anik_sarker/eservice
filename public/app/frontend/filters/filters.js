'use strict';

/* Filters */
// need load the moment.js to use this filter. 
angular.module('app.filters', [])
    .filter('fromNow', function() {
        return function(date) {
          return moment(date).fromNow();
        }
    })
    .filter('capitalize', function() {
        return function(input, scope) {
            if (input!=null)
                input = input.toLowerCase();
            return input.substring(0,1).toUpperCase()+input.substring(1);
        }
    })
    .filter('ageConvert', function() {        
        return function(birthdate) {            
            var ageDifMs = Date.now() -new Date(birthdate).getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);           
        }
    })
    ;